package auth

import (
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"gorm.io/gorm"
	"log"
	support "minhho/helpers"
	"minhho/oauth2/internal/acl/models"
	"time"
)

type Auth struct {
	db          *gorm.DB
	User        models.User
	credentials *Credentials
	payload     *CustomClaims
	method      jwt.SigningMethod
	key         struct {
		private string
		public  string
	}
}

func NewAuth(c *gin.Context, db *gorm.DB) *Auth {
	credentials := new(Credentials)
	if data := credentials.Validated(c); data != nil {
		return &Auth{
			db:          db,
			credentials: data,
		}
	}
	return nil
}

func (auth *Auth) SetMethod(method string) *Auth {
	switch method {
	case "HS256":
		auth.method = jwt.SigningMethodHS256
	case "HS384":
		auth.method = jwt.SigningMethodHS384
	case "HS512":
		auth.method = jwt.SigningMethodHS512
	case "ES256":
		auth.method = jwt.SigningMethodES256
	case "ES384":
		auth.method = jwt.SigningMethodES384
	case "ES512":
		auth.method = jwt.SigningMethodES512
	}
	return auth
}

func (auth *Auth) SetKey(private string, public string) *Auth {
	auth.key.private = private
	auth.key.public = public
	return auth
}

func (auth *Auth) generateJWT() string {
	secret := []byte(auth.key.private)
	token := jwt.NewWithClaims(auth.method, auth.payload)
	tokenString, err := token.SignedString(secret)
	if err != nil {
		log.Print("JWT token generate false.")
	}
	return tokenString
}

func (auth *Auth) attempt() {
	var user models.User
	result := auth.db.Where("email = ?", auth.credentials.Email).First(&user)
	if result.RowsAffected > 0 && support.CheckPasswordHash(auth.credentials.Password, user.Password) {
		auth.User = user
		auth.payload = NewClaims(user, time.Now().Add(time.Hour*24).Unix())
		auth.generateJWT()
	}
}
