package auth

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Credentials struct {
	Email    string `json:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

func (credentials *Credentials) Validated(c *gin.Context) *Credentials {
	if err := c.ShouldBind(&credentials); err != nil {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"messages": err.Error(),
		})
		return nil
	}
	return credentials
}
