package auth

import (
	"github.com/golang-jwt/jwt"
	"minhho/oauth2/internal/acl/models"
	"time"
)

type CustomClaims struct {
	Name      string    `json:"name"`
	Uid       string    `json:"uid"`
	Email     string    `json:"email"`
	UpdatedAt time.Time `json:"updated_at"`
	jwt.StandardClaims
}

func NewClaims(user models.User, expiresAt int64) *CustomClaims {
	return &CustomClaims{
		user.FirstName + " " + user.LastName,
		user.UID.String(),
		user.Email,
		user.UpdatedAt,
		jwt.StandardClaims{
			ExpiresAt: expiresAt,
			Issuer:    user.UID.String(),
			IssuedAt:  time.Now().Unix(),
		},
	}
}
