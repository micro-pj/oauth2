package migrations

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/oauth/models"
	"reflect"
)

type AccessTokenTable struct {
	Version int
	Connect *gorm.DB
}

func (m *AccessTokenTable) Up() error {
	err := m.Connect.AutoMigrate(&models.AccessToken{})
	return err
}

func (m *AccessTokenTable) Down() error {
	err := m.Connect.Migrator().DropTable(&models.AccessToken{})
	return err
}

func (m *AccessTokenTable) GetStructName() string {
	if t := reflect.TypeOf(m); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
