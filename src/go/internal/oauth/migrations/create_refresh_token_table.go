package migrations

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/oauth/models"
	"reflect"
)

type RefreshTokenTable struct {
	Version int
	Connect *gorm.DB
}

func (m *RefreshTokenTable) Up() error {
	err := m.Connect.AutoMigrate(&models.RefreshToken{})
	return err
}

func (m *RefreshTokenTable) Down() error {
	err := m.Connect.Migrator().DropTable(&models.RefreshToken{})
	return err
}

func (m *RefreshTokenTable) GetStructName() string {
	if t := reflect.TypeOf(m); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
