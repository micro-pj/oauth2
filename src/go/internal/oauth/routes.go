package oauth

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"minhho/oauth2/configs"
	"minhho/oauth2/internal/oauth/handlers"
)

func Routes(e *gin.RouterGroup, c *configs.Config, db *gorm.DB) *gin.RouterGroup {
	auth := handlers.NewAuthHandler(c, db)
	e.POST("login", auth.Login)
	return e
}
