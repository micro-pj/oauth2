package models

import (
	"gorm.io/gorm"
	"time"
)

type RefreshToken struct {
	gorm.Model
	AccessTokenId int
	AccessToken   AccessToken `gorm:"foreignKey:AccessTokenId"`
	Status        bool        `gorm:"default:true;index"`
	ExpiresAt     time.Time
}
