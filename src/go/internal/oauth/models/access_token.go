package models

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/acl/models"
	"time"
)

type AccessToken struct {
	gorm.Model
	Name      string `gorm:"not null"`
	UserId    int
	User      models.User `gorm:"foreignKey:UserId"`
	ClientID  int
	Client    models.Client `gorm:"foreignKey:ClientID"`
	Status    bool          `gorm:"default:true;index"`
	ExpiresAt time.Time
}
