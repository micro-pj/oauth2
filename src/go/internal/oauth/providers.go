package oauth

import (
	"gorm.io/gorm"
	migrate "minhho/migrations"
	oauth "minhho/oauth2/internal/oauth/migrations"
)

func LoadMigration(db *gorm.DB) []migrate.MigrationContract {
	return []migrate.MigrationContract{
		&oauth.AccessTokenTable{Connect: db},
		&oauth.RefreshTokenTable{Connect: db},
	}
}
