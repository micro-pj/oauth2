package models

import "gorm.io/gorm"

type Permission struct {
	gorm.Model
	Name        string `gorm:"size:255,not null,uniqueIndex"`
	DisplayName string `gorm:"size:255" json:"display_name"`
	Description string
	Roles       []Role `gorm:"many2many:roles_permissions"`
}
