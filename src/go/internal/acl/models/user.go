package models

import (
	"github.com/gin-gonic/gin"
	"github.com/gofrs/uuid"
	"gorm.io/gorm"
	"net/http"
)

type User struct {
	gorm.Model
	UID         uuid.UUID `gorm:"index"`
	Email       string    `gorm:"not null;uniqueIndex"`
	FirstName   string    `gorm:"size:255;not null"`
	LastName    string    `gorm:"size:255;not null"`
	Password    string    `gorm:"size:255;not null"`
	Status      bool      `gorm:"default:true;index"`
	IsAdmin     bool      `gorm:"default:false"`
	ClientIp    string    `gorm:"size:50"`
	ClientAgent string
	Roles       []Role   `gorm:"many2many:users_roles"`
	Clients     []Client `gorm:"many2many:clients_users"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.UID, _ = uuid.NewV4()
	return
}

func (u *User) Validated(c *gin.Context) *User {
	if err := c.ShouldBind(&u); err != nil {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"messages": err.Error(),
		})
		return nil
	}
	return u
}
