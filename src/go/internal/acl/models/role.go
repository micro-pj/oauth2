package models

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
)

type Role struct {
	gorm.Model
	Name        string       `gorm:"size:255,not null,uniqueIndex" json:"name"`
	Description string       `json:"description"`
	Users       []User       `gorm:"many2many:users_roles"`
	Permissions []Permission `gorm:"many2many:roles_permissions" json:"permissions,omitempty"`
}

func (r *Role) Validated(c *gin.Context) *Role {
	if err := c.ShouldBind(&r); err != nil {
		c.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"messages": err.Error(),
		})
		return nil
	}
	return r
}
