package models

import (
	"gorm.io/gorm"
)

type Client struct {
	gorm.Model
	Users    []User `gorm:"many2many:clients_users"`
	Name     string `gorm:"size:255,not null"`
	Secret   string `gorm:"size:100"`
	Redirect string
	Status   bool `gorm:"default:true;index"`
}
