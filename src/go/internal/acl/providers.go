package acl

import (
	"gorm.io/gorm"
	migrate "minhho/migrations"
	acl "minhho/oauth2/internal/acl/migrations"
)

func LoadMigration(db *gorm.DB) []migrate.MigrationContract {
	return []migrate.MigrationContract{
		&acl.ClientTable{Connect: db},
		&acl.UserTable{Connect: db},
		&acl.RoleTable{Connect: db},
		&acl.PermissionTable{Connect: db},
	}
}
