package migrations

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/acl/models"
	"reflect"
)

type PermissionTable struct {
	Version int
	Connect *gorm.DB
}

func (m *PermissionTable) Up() error {
	err := m.Connect.AutoMigrate(&models.Permission{})
	return err
}

func (m *PermissionTable) Down() error {
	err := m.Connect.Migrator().DropTable(&models.Permission{})
	return err
}

func (m *PermissionTable) GetStructName() string {
	if t := reflect.TypeOf(m); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
