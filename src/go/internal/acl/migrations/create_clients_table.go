package migrations

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/acl/models"
	"reflect"
)

type ClientTable struct {
	Version int
	Connect *gorm.DB
}

func (m *ClientTable) Up() error {
	err := m.Connect.AutoMigrate(&models.Client{})
	return err
}

func (m *ClientTable) Down() error {
	err := m.Connect.Migrator().DropTable("clients_users")
	err = m.Connect.Migrator().DropTable(&models.Client{})
	return err
}

func (m *ClientTable) GetStructName() string {
	if t := reflect.TypeOf(m); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
