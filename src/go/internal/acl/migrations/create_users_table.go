package migrations

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/acl/models"
	"reflect"
)

type UserTable struct {
	Version int
	Connect *gorm.DB
}

func (m *UserTable) Up() error {
	err := m.Connect.AutoMigrate(&models.User{})
	return err
}

func (m *UserTable) Down() error {
	err := m.Connect.Migrator().DropTable("users_roles")
	err = m.Connect.Migrator().DropTable(&models.User{})
	return err
}

func (m *UserTable) GetStructName() string {
	if t := reflect.TypeOf(m); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
