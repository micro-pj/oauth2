package migrations

import (
	"gorm.io/gorm"
	"minhho/oauth2/internal/acl/models"
	"reflect"
)

type RoleTable struct {
	Version int
	Connect *gorm.DB
}

func (m *RoleTable) Up() error {
	err := m.Connect.AutoMigrate(&models.Role{})
	return err
}

func (m *RoleTable) Down() error {
	err := m.Connect.Migrator().DropTable("roles_permissions")
	err = m.Connect.Migrator().DropTable(&models.Role{})
	return err
}

func (m *RoleTable) GetStructName() string {
	if t := reflect.TypeOf(m); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
