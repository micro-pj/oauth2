package handlers

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	controller "minhho/handers"
	"minhho/oauth2/configs"
)

type PermissionHandler struct {
	config *configs.Config
	*controller.Handler
}

func NewPermissionHandler(c *configs.Config, db *gorm.DB) *PermissionHandler {
	handler := controller.NewHandler(db)
	return &PermissionHandler{
		c,
		handler,
	}
}

func (r *PermissionHandler) Index(c *gin.Context) {
	return
}

func (r *PermissionHandler) Show(c *gin.Context) {

}

func (r *PermissionHandler) Store(c *gin.Context) {
	return
}

func (r *PermissionHandler) Update(c *gin.Context) {
	return
}

func (r *PermissionHandler) Destroy(c *gin.Context) {
	return
}
