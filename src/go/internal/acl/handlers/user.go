package handlers

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	controller "minhho/handers"
	"minhho/oauth2/configs"
)

type UserHandler struct {
	config *configs.Config
	*controller.Handler
}

func NewUserHandler(c *configs.Config, db *gorm.DB) *UserHandler {
	handler := controller.NewHandler(db)
	return &UserHandler{
		c,
		handler,
	}
}

func (r *UserHandler) Index(c *gin.Context) {
	return
}

func (r *UserHandler) Show(c *gin.Context) {

}

func (r *UserHandler) Store(c *gin.Context) {
	return
}

func (r *UserHandler) Update(c *gin.Context) {
	return
}

func (r *UserHandler) Destroy(c *gin.Context) {
	return
}
