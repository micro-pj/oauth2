package handlers

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	controller "minhho/handers"
	"minhho/oauth2/configs"
	"minhho/oauth2/internal/acl/models"
	"minhho/oauth2/internal/acl/transforms"
	"net/http"
)

type RoleHandler struct {
	config *configs.Config
	t      *transforms.RoleTransform
	*controller.Handler
}

func NewRoleHandler(c *configs.Config, db *gorm.DB) *RoleHandler {
	handler := controller.NewHandler(db)
	t := new(transforms.RoleTransform)
	return &RoleHandler{
		c,
		t,
		handler,
	}
}

func (r *RoleHandler) Index(c *gin.Context) {
	var roles []models.Role
	r.Db.Preload("Permissions").Find(&roles)
	c.JSON(http.StatusOK, r.t.Collection(roles))
	return
}

func (r *RoleHandler) Show(c *gin.Context) {
	roleId := c.Param("id")
	var role models.Role
	r.Db.Preload("Permissions").First(&role, roleId)
	c.JSON(http.StatusOK, r.t.Data(role))
}

func (r *RoleHandler) Store(c *gin.Context) {
	request := new(models.Role)
	if data := request.Validated(c); data != nil {
		_ = r.Db.Transaction(func(tx *gorm.DB) error {
			if err := tx.Create(&data).Error; err != nil {
				return err
			}
			return nil
		})
	}
	return
}

func (r *RoleHandler) Update(c *gin.Context) {
	return
}

func (r *RoleHandler) Destroy(c *gin.Context) {
	return
}
