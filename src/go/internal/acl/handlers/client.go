package handlers

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	controller "minhho/handers"
	"minhho/oauth2/configs"
)

type ClientHandler struct {
	config *configs.Config
	*controller.Handler
}

func NewClientHandler(c *configs.Config, db *gorm.DB) *ClientHandler {
	handler := controller.NewHandler(db)
	return &ClientHandler{
		c,
		handler,
	}
}

func (r *ClientHandler) Index(c *gin.Context) {
	return
}

func (r *ClientHandler) Show(c *gin.Context) {

}

func (r *ClientHandler) Store(c *gin.Context) {
	return
}

func (r *ClientHandler) Update(c *gin.Context) {
	return
}

func (r *ClientHandler) Destroy(c *gin.Context) {
	return
}
