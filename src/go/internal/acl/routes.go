package acl

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"minhho/oauth2/configs"
	"minhho/oauth2/internal/acl/handlers"
)

func Routes(e *gin.RouterGroup, c *configs.Config, db *gorm.DB) *gin.RouterGroup {
	role := handlers.NewRoleHandler(c, db)
	routes := e.Group("roles")
	{
		routes.GET("/", role.Index)
		routes.GET("/:id", role.Show)
		routes.POST("/", role.Store)
		routes.PUT("/:id", role.Update)
		routes.DELETE("/", role.Update)
	}

	user := handlers.NewUserHandler(c, db)
	routes = e.Group("users")
	{
		routes.GET("/", user.Index)
		routes.GET("/:id", user.Show)
		routes.POST("/", user.Store)
		routes.PUT("/:id", user.Update)
		routes.DELETE("/", user.Update)
	}

	per := handlers.NewPermissionHandler(c, db)
	routes = e.Group("permissions")
	{
		routes.GET("/", per.Index)
		routes.GET("/:id", per.Show)
		routes.POST("/", per.Store)
		routes.PUT("/:id", per.Update)
		routes.DELETE("/", per.Update)
	}

	client := handlers.NewPermissionHandler(c, db)
	routes = e.Group("clients")
	{
		routes.GET("/", client.Index)
		routes.GET("/:id", client.Show)
		routes.POST("/", client.Store)
		routes.PUT("/:id", client.Update)
		routes.DELETE("/", client.Update)
	}
	return e
}
