package transforms

import (
	"encoding/json"
	"fmt"
	support "minhho/helpers"
	"minhho/oauth2/internal/acl/models"
)

type PermissionTransform struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	Description string `json:"description"`
}

func (t PermissionTransform) Data(permission models.Permission) PermissionTransform {
	jsonCover := support.StructToJson(permission)
	if err := json.Unmarshal(jsonCover, &t); err != nil {
		fmt.Println(err)
	}
	return t
}

func (t PermissionTransform) Collection(pers []models.Permission) []PermissionTransform {
	var data []PermissionTransform
	for _, per := range pers {
		data = append(data, t.Data(per))
	}
	return data
}
