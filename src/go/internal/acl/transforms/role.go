package transforms

import (
	"encoding/json"
	"fmt"
	support "minhho/helpers"
	"minhho/oauth2/internal/acl/models"
)

type RoleTransform struct {
	ID          int                   `json:"id"`
	Name        string                `json:"name"`
	Description string                `json:"description"`
	Permissions []PermissionTransform `json:"permissions"`
}

func (t RoleTransform) Data(role models.Role) RoleTransform {
	jsonCover := support.StructToJson(role)
	if err := json.Unmarshal(jsonCover, &t); err != nil {
		fmt.Println(err)
	}
	return t
}

func (t RoleTransform) Collection(roles []models.Role) []RoleTransform {
	var data []RoleTransform
	for _, role := range roles {
		data = append(data, t.Data(role))
	}
	return data
}
