package _go

import (
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

func ContainsSlice(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

func StructToJson(item interface{}) []byte {
	b, err := json.Marshal(item)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return b
}

func HashPassword(password string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes)
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
