package _go

import "gorm.io/gorm"

type MigrationContract interface {
	Up() error
	Down() error
	GetStructName() string
}

type Migration struct {
	gorm.Model
	Name    string `gorm:"size:255,not null,uniqueIndex"`
	Version int
}
