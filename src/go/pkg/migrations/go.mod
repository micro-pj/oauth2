module minhho/migrations

go 1.17

require (
	gorm.io/gorm v1.23.4
	minhho/helpers v1.0.0
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
)

replace (
	minhho/helpers => ../helpers
)
