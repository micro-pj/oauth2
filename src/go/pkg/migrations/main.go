package _go

import (
	"fmt"
	"gorm.io/gorm"
	support "minhho/helpers"
)

type Migrate struct {
	Db *gorm.DB
}

func (m Migrate) Init() Migrate {
	if !m.Db.Migrator().HasTable("migrations") {
		if err := m.Db.Migrator().CreateTable(&Migration{}); err != nil {
			fmt.Printf("Create Migration Table False")
		}
	}
	return m
}

func (m Migrate) getLastVersion() int {
	var lastMigrate Migration
	m.Db.Last(&Migration{}).Scan(&lastMigrate)
	return lastMigrate.Version
}

func (m Migrate) getMigrated() []string {
	var names []string
	m.Db.Model(&Migration{}).Pluck("Name", &names)
	return names
}

func (m Migrate) Migrate(migrations []MigrationContract) {
	version := m.getLastVersion() + 1
	migrated := m.getMigrated()
	var ms []Migration
	for _, element := range migrations {
		if !support.ContainsSlice(migrated, element.GetStructName()) {
			err := element.Up()
			if err != nil {
				fmt.Printf("Migrate " + element.GetStructName() + " False: " + err.Error())
			} else {
				ms = append(ms, Migration{Name: element.GetStructName(), Version: version})
			}
		}
	}
	m.Db.CreateInBatches(ms, 20)
}

func (m Migrate) getMigrationByVersion(version int) []string {
	var names []string
	m.Db.Model(&Migration{}).Where(Migration{Version: version}).Pluck("Name", &names)
	return names
}

func (m Migrate) Rollback(migrations []MigrationContract) {
	version := m.getLastVersion()
	migrationRollback := m.getMigrationByVersion(version)
	var migrationsRollbackSc []string
	for _, element := range migrations {
		if support.ContainsSlice(migrationRollback, element.GetStructName()) {
			err := element.Down()
			if err != nil {
				fmt.Printf("Migrate Rollback " + element.GetStructName() + " False: " + err.Error())
			} else {
				migrationsRollbackSc = append(migrationsRollbackSc, element.GetStructName())
			}
		}
	}
	m.Db.Where("name IN ?", migrationsRollbackSc).Unscoped().Delete(&Migration{})
}
