package _go

import (
	"minhho/env/drivers"
)

type Builder interface {
	SetPath(path string)
	Build() []byte
}

func NewBuilder(builderType string) Builder {
	switch builderType {
	case "env":
		return &drivers.EnvBuilder{}
	case "yml":
		return &drivers.YmlBuilder{}
	default:
		return nil
	}
}
