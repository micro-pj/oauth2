module minhho/env

go 1.17

require (
	github.com/joho/godotenv v1.4.0
	gopkg.in/yaml.v2 v2.4.0
)
