package helpers

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Root       = filepath.Join(filepath.Dir(b), "../../..")
)

func ValidateConfigPath(configPath string) error {
	s, err := os.Stat(configPath)
	if err != nil {
		return err
	}

	if s.IsDir() {
		return fmt.Errorf("'%s' is a directiory, not a file", configPath)
	}

	return nil
}

func Decode(i interface{}) {
	var t reflect.Type
	var v reflect.Value
	var ok bool

	if v, ok = i.(reflect.Value); !ok {
		v = reflect.ValueOf(i).Elem()
	}

	t = v.Type()

	for i := 0; i < v.NumField(); i++ {
		if v.CanInterface() {
			if v.Field(i).Kind() != reflect.Struct {
				if eTag, ok := t.Field(i).Tag.Lookup("env"); ok {
					eVal := reflect.ValueOf(os.Getenv(eTag))
					if eVal.IsValid() {
						v.Field(i).Set(eVal.Convert(t.Field(i).Type))
					}
				}
			} else if v.Field(i).Kind() == reflect.Struct {
				Decode(v.Field(i))
			} else {
				panic("can not reflect value:" + v.Field(i).Kind().String())
			}
		}
	}
}
