package drivers

import (
	"encoding/json"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"minhho/env/helpers"
)

type YmlBuilder struct {
	Path string
}

func (c *YmlBuilder) SetPath(path string) {
	newPath := helpers.Root + "/" + path
	if err := helpers.ValidateConfigPath(newPath); err != nil {
		log.Printf("Config File path is can not be loaded: %v\n", err)
	} else {
		c.Path = newPath
	}
}

func (c *YmlBuilder) Build() []byte {
	file, err := ioutil.ReadFile(c.Path)
	if err != nil {
		log.Fatalf(err.Error())
	}
	data := make(map[string]map[string]string)
	err = yaml.Unmarshal(file, &data)
	log.Print(data)
	if err != nil {
		log.Fatal(err)
	}
	jsonStr, err := json.Marshal(data)
	if err != nil {
		log.Fatalf(err.Error())
		return nil
	}
	return jsonStr
}
