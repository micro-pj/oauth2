package drivers

import (
	"encoding/json"
	"github.com/joho/godotenv"
	"log"
	"minhho/env/helpers"
)

type EnvBuilder struct {
	Path string
}

func (c *EnvBuilder) SetPath(path string) {
	newPath := helpers.Root + "/" + path
	if err := helpers.ValidateConfigPath(newPath); err != nil {
		log.Printf("Config File path is can not be loaded: %v\n", err)
	}
	c.Path = newPath
}

func (c *EnvBuilder) Build() []byte {
	data, err := godotenv.Read(c.Path)
	if err != nil {
		log.Fatalf(err.Error())
	}
	jsonStr, err := json.Marshal(data)
	if err != nil {
		log.Fatalf(err.Error())
		return nil
	}
	return jsonStr
}
