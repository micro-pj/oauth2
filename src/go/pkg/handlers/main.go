package _go

import (
	"context"
	"gorm.io/gorm"
)

type Handler struct {
	ctx context.Context
	Db  *gorm.DB
}

func NewHandler(db *gorm.DB) *Handler {
	return &Handler{Db: db}
}
