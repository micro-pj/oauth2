package connection

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"minhho/db/config"
)

type MySql struct {
	Info config.DB
}

func (conn MySql) ConnectString() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		conn.Info.User, conn.Info.Pass, conn.Info.Host, conn.Info.Port, conn.Info.Database)
}

func (conn MySql) Connect() *gorm.DB {
	SqlInfo := conn.ConnectString()
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       SqlInfo,
		DefaultStringSize:         256,
		DisableDatetimePrecision:  true,
		DontSupportRenameIndex:    true,
		SkipInitializeWithVersion: false,
	}), &gorm.Config{})
	if err != nil {
		fmt.Println("Connect to MySql DB fails.")
	}
	return db
}
