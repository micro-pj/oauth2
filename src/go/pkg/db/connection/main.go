package connection

import "gorm.io/gorm"

type Connection interface {
	ConnectString() string
	Connect() *gorm.DB
}
