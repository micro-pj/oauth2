package connection

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"minhho/db/config"
)

type Postgres struct {
	Info config.DB
}

func (conn Postgres) ConnectString() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		conn.Info.Host, conn.Info.Port, conn.Info.User, conn.Info.Pass, conn.Info.Database)
}

func (conn Postgres) Connect() *gorm.DB {
	psqlInfo := conn.ConnectString()
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  psqlInfo,
		PreferSimpleProtocol: true,
	}), &gorm.Config{})
	if err != nil {
		fmt.Println("Connect to Postgres DB fails.")
	}
	return db
}
