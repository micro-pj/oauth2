package _go

import (
	"encoding/json"
	"fmt"
	db "minhho/db/config"
	"minhho/db/connection"
	config "minhho/env"
)

type DBConn struct {
	driver string
	info   db.DB
}

func (c DBConn) SetDriver(driver string) DBConn {
	c.driver = driver
	return c
}

func (c DBConn) LoadInfo(env string, path string) DBConn {
	builder := config.NewBuilder(env)
	builder.SetPath(path)
	data := builder.Build()
	database := db.DB{}

	if err := json.Unmarshal(data, &database); err != nil {
		fmt.Println(err)
	}
	c.info = database
	return c
}

func (c DBConn) GetDriver() connection.Connection {
	switch c.driver {
	case "postgres":
		return &connection.Postgres{Info: c.info}
	case "mysql":
		return &connection.MySql{Info: c.info}
	default:
		return nil
	}
}
