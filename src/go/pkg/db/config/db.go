package config

type DB struct {
	Host     string `json:"DB_HOST"`
	Port     string `json:"DB_PORT"`
	Database string `json:"DB_DATABASE"`
	User     string `json:"DB_USER"`
	Pass     string `json:"DB_PASSWORD"`
}
