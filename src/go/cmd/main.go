package main

import (
	_go "minhho/oauth2"
	"minhho/oauth2/configs"
)

func main() {
	c := configs.Load()
	application := _go.NewApp(c)
	application.Run()
}
