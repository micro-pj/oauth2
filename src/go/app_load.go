package _go

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	_go "minhho/db"
	support "minhho/helpers"
	migrate "minhho/migrations"
	"minhho/oauth2/configs"
	"minhho/oauth2/internal/acl"
	"minhho/oauth2/internal/acl/models"
	"minhho/oauth2/internal/oauth"
	"net/http"
)

func LoadDB() *gorm.DB {
	conn := _go.DBConn{}
	return conn.SetDriver("postgres").LoadInfo("env", ".env").GetDriver().Connect()
}

func LoadRoutes(c *configs.Config, db *gorm.DB) *gin.Engine {
	r := gin.New()
	r.GET("/health_check", func(c *gin.Context) {
		c.AbortWithStatus(http.StatusOK)
		return
	})
	api := r.Group("/api")
	{
		aclR := api.Group("acl")
		{
			acl.Routes(aclR, c, db)
		}

		oauthR := api.Group("oauth")
		{
			oauth.Routes(oauthR, c, db)
		}
	}
	return r
}

func Migrate(db *gorm.DB) {
	m := migrate.Migrate{Db: db}
	var migrations []migrate.MigrationContract
	migrations = append(migrations, acl.LoadMigration(db)...)
	migrations = append(migrations, oauth.LoadMigration(db)...)
	m.Init().Migrate(migrations)
}

func MigrateRollback(db *gorm.DB) {
	m := migrate.Migrate{Db: db}
	var migrations []migrate.MigrationContract
	migrations = append(migrations, acl.LoadMigration(db)...)
	migrations = append(migrations, oauth.LoadMigration(db)...)
	m.Init().Rollback(migrations)
}

func InitUser(db *gorm.DB) {
	role := models.Role{
		Name:        "Admin",
		Description: "Admin Users Role",
		Users: []models.User{
			{
				Email:     "admin@minhho.net",
				IsAdmin:   true,
				FirstName: "Minh",
				LastName:  "Ho",
				Password:  support.HashPassword("123456Aa@"),
			},
		},
	}

	db.FirstOrCreate(&role)
	db.Save(&role)
}
