package _go

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"log"
	"minhho/oauth2/configs"
	"net/http"
	"time"
)

type App struct {
	ctx    context.Context
	server *http.Server
	config *configs.Config
	db     *gorm.DB
}

func NewApp(config *configs.Config) *App {
	db := LoadDB()
	if config.AppEnv == "production" {
		gin.SetMode(gin.ReleaseMode)
	}
	c := context.Background()
	r := LoadRoutes(config, db)
	server := &http.Server{
		Addr:         fmt.Sprintf("%s:%s", config.Server.Host, config.Server.Port),
		Handler:      r,
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  30 * time.Second,
	}
	return &App{
		ctx:    c,
		config: config,
		server: server,
		db:     db,
	}
}

func (app *App) Run() {
	log.Printf("Listening on %s:%v...\n", app.config.Server.Host, app.config.Server.Port)
	if err := app.server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
