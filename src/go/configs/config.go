package configs

import (
	"encoding/json"
	"fmt"
	config "minhho/env"
)

type Server struct {
	Host string `json:"APP_HOST"`
	Port string `json:"APP_PORT"`
}

type KEY struct {
	Public  string `json:"PUBLIC_KEY"`
	Private string `json:"PRIVATE_KEY"`
}

type Config struct {
	AppEnv        string `json:"APP_ENV"`
	AutoMigration string `json:"AUTO_MIGRATION"`
	Server
	KEY
}

func Load() *Config {
	builder := config.NewBuilder("env")
	builder.SetPath(".env")
	data := builder.Build()
	c := Config{}
	if err := json.Unmarshal(data, &c); err != nil {
		fmt.Println(err)
	}
	return &c
}
