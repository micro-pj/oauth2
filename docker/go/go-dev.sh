#!/bin/sh

go get -u github.com/gin-gonic/gin
go get github.com/joho/godotenv
go get github.com/golang-jwt/jwt
go get gopkg.in/yaml.v2
go get -u gorm.io/gorm
go get gorm.io/driver/mysql
go get gorm.io/driver/postgres

go mod download

go install github.com/cosmtrek/air@latest

go mod tidy
air